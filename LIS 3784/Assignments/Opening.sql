-- upload data structures and data
drop database if exists rnr06;
create database if not exists rnr06;
-- use database rnr06
use rnr06;
-- show all tables in rnr06 database
show tables;
-- load premiere.sql (or use \. db/premiere.sql
source db/premiere.sql
-- describe each table
describe customer;
-- select all records from customer table
select * from customer;
--P2 SQL Solutions
/*
Notes: *ALL* tables *must* include the following checks and defaults:
    state: default = FL
    *all* currency values > 0
    *all* enum values use w/in "IN" clause
    zip: > and <= 000000000

*ALL* FK's: *Must* require ON DELETE RESTRICT, ON UPDATE CASCADE
*/

--(not the same, but) similar to SHOW WARNINGS;
set ansi_warnings on;
GO

use master;
GO

--drop existing database if it exists (use *your* username)
if exists (select name from master.dbo.sysdatabases where name = N'rnr06')
drop database rnr06;
GO

--create database if not exists (use *your* username)
if not exists (select name from master.dbo.sysdatabases where name = N'rnr06')
create database rnr06;
GO

use rnr06;
GO

-------------------------------------------------------------------------------
--Table dbo.patient
-------------------------------------------------------------------------------
--drop table if exists
--N=subsequent string may be in Unicode (makes it portable to use with Unicode characters)
--U=only look for objects with this name that are tables
--*be sure* to use dbo. before *all* table references
if object_id (N'dbo.patient', N'U') is not NULL
drop table dbo.patient;
GO

create table dbo.patient
(
    pat_id smallint not null identity(1,1),
    pat_ssn int not null check (pat_ssn > 0 and pat_ssn <= 999999999),
    pat_fname varchar(15) not null,
    pat_lname varchar(30) not null,
    pat_street varchar(30) not null,
    pat_city varchar(30) not null,
    pat_state char(2) not null default 'FL',
    pat_zip int not null check (pat_zip > 0 and pat_zip <= 999999999),
    pat_phone bigint not null check (pat_phone > 0 and pat_phone <= 9999999999),
    pat_email varchar(100) null,
    pat_dob date not null,
    pat_gender char(1) not null check (pat_gender IN('M','F')),
    pat_notes varchar(45) null,
    primary key (pat_id),

    --make sure SSNs and State IDs are unique
    constraint ux_pat_ssn unique nonclustered (pat_ssn ASC)
);

--nonclustered by default

-------------------------------------------------------------------------------
--Table dbo.medication
-------------------------------------------------------------------------------
if object_id (N'dbo.medication', N'U') is not NULL
drop table dbo.medication;

create table dbo.medication
(
    med_id smallint not null identity(1,1),
    med_name varchar(100) not null,
    med_price decimal(5,2) not null check (med_price > 0),
    med_shelf_life date not null,
    med_notes varchar(255),
    primary key (med_id)
);

-------------------------------------------------------------------------------
--Table dbo.prescription
-------------------------------------------------------------------------------
if object_id (N'dbo.prescription', N'U') is not NULL
drop table dbo.prescription;

create table dbo.prescription
(
    pre_id smallint not null identity(1,1),
    pat_id smallint not null,
    med_id smallint not null,
    pre_date date not null,
    pre_dosage varchar(255) not null,
    pre_num_refills varchar(3) not null,
    pre_notes varchar(255) null,
    primary key (pre_id),

    --make sure combination of pat_id, med_id, and pre_date is unique
    constraint ux_pat_id_med_id_pre_date unique NONCLUSTERED
    (pat_id ASC, med_id ASC, pre_date ASC),

    constraint fk_prescription_patient
        foreign key (pat_id)
        references dbo.patient (pat_id)
        on delete no ACTION
        on update cascade,

    constraint fk_prescription_medication
        foreign key (med_id)
        references dbo.medication (med_id)
        on delete no ACTION
        on update cascade
);

-------------------------------------------------------------------------------
--Table dbo.treatment
-------------------------------------------------------------------------------
if object_id (N'dbo.treatment', N'U') is not NULL
drop table dbo.treatment;

create table dbo.treatment
(
    trt_id smallint not null identity(1,1),
    trt_name varchar(255) not null,
    trt_price  decimal(8,2) not null check (trt_price > 0),
    trt_notes varchar(255) null,
    primary key (trt_id)
);

-------------------------------------------------------------------------------
--Table dbo.physician
-------------------------------------------------------------------------------
if object_id (N'dbo.physician', N'U') is not NULL
drop table dbo.physician;

create table dbo.physician
(
    phy_id smallint not null identity(1,1),
    phy_speciality varchar(25) not null,
    phy_fname varchar(15) not null,
    phy_lname varchar(30) not null,
    phy_street varchar(30) not null,
    phy_city varchar(30) not null,
    phy_state char(2) not null default 'FL',
    phy_zip int not null check (phy_zip > 0 and phy_zip <= 999999999),
    phy_phone bigint not null check (phy_phone > 0 and phy_phone <= 9999999999),
    phy_fax bigint not null check (phy_fax > 0 and phy_fax <= 9999999999),
    phy_email varchar(100) null,
    phy_url varchar(100) null,
    phy_notes varchar(255) null,
    primary key (phy_id)
);

-------------------------------------------------------------------------------
--Table dbo.patient_treatment
-------------------------------------------------------------------------------
if object_id (N'dbo.patient_treatment', N'U') is not NULL
drop table dbo.patient_treatment;

create table dbo.patient_treatment
(
    ptr_id smallint not null identity(1,1),
    pat_id smallint not null,
    phy_id smallint not null,
    trt_id smallint not null,
    ptr_date date not null,
    ptr_start time(0) not null,
    ptr_end time(0) not null,
    ptr_results varchar(255) null,
    ptr_notes varchar(255) null,
    primary key (ptr_id),

    --make sure combination of pat_id, phy_id, trt_id, and prt_date is unique
    constraint ux_pat_id_phy_id_trt_id_ptr_date unique NONCLUSTERED
    (pat_id ASC, phy_id ASC, trt_id ASC, ptr_date ASC),

    constraint fk_patient_treatment_patient
        foreign key (pat_id)
        references dbo.patient (pat_id)
        on delete no ACTION
        on update cascade,

    constraint fk_patient_treatment_physician
        foreign key (phy_id)
        references dbo.physician (phy_id)
        on delete no ACTION
        on update cascade,

    CONSTRAINT fk_patient_treatment_treatment
        foreign key (trt_id)
        references dbo.treatment (trt_id)
        on delete no ACTION
        on update cascade
);

-------------------------------------------------------------------------------
--Table dbo.administration.lu
-------------------------------------------------------------------------------
if object_id (N'dbo.administration_lu', N'U') is not NULL
drop table dbo.administration_lu;

create table dbo.administration_lu
(
    pre_id smallint not null,
    ptr_id smallint not null,
    primary key (pre_id, ptr_id),
    
    --either one of these FKs may be ON DELETE/ON UPDATE CASCADE, but *not* both!
    --if both FKs contain CASCADE, generate "may cause cycles or multiple cascade paths" error!
    CONSTRAINT fk_administration_lu_prescription
        foreign key (pre_id)
        references dbo.prescription (pre_id)
        on delete no ACTION
        on update cascade,

    CONSTRAINT fk_administration_lu_patient_treatment
        foreign key (ptr_id)
        references dbo.patient_treatment (ptr_id)
        on DELETE no ACTION
        on update no ACTION
);

--show tables;
select * from information_schema.tables;

--disable all contraints (must do this *after*  table creation, but *before* inserts)
EXEC sp_msforeachtable "alter table ? nocheck constraint all"

-------------------------------------------------------------------------------
--Data for table dbo.patient
-------------------------------------------------------------------------------
insert into dbo.patient
(pat_ssn, pat_fname, pat_lname, pat_street, pat_city, pat_state, pat_zip, pat_phone, pat_email,
pat_dob, pat_gender, pat_notes)

--notice: SQL Server accepts dates in various formats

values
('123452582', 'Amanda', 'Thomas', '5261 2nd Avenue', 'Tallahassee', 'FL', '323035480', 8506524235, 'a_thomas@gmail.com', '05-24-2001', 'F', null),
('526751035', 'Eric', 'Nollin', '3941  Kenwood Place', 'Orlando', 'FL', '328015402', 9545558134, 'ericnollin@gmail.com', '06-19-2000', 'M', null),
('523012452', 'Jason', 'Bragg', '3509 Ten Mile Road', 'Fleming Island', 'FL', '320038659', 4074670841, 'jkb05@my.fsu.edu', '10-27-1997', 'M', null),
('231562324', 'Marie', 'Smith', '400 Evergreen Avenue', 'Deerfield Beach', 'FL', '334421067', 9548169096, 'marie_smith@comcast.net', '07-07-1986', 'F', null),
('352128946', 'Annabelle', 'Weston', '1801 Kenwood Place', 'Fort Lauderdale', 'FL', '333065426', 5614560671, 'annieweston@gmail.com', '04-25-1989', 'F', null);

-------------------------------------------------------------------------------
--Data for table dbo.medication
-------------------------------------------------------------------------------
insert into dbo.medication
(med_name, med_price, med_shelf_life, med_notes)

values
('Adderall', 150.00, '10-30-2012', null),
('Ativan', 225.00, '06-15-2017', null),
('Doxycycline', 95.00, '09-22-2014', null),
('Nasocort', 75.00, '11-03-2019', null),
('Singular', 125.00, '02-26-2016', null);

-------------------------------------------------------------------------------
--Data for table dbo.prescription
-------------------------------------------------------------------------------
insert into dbo.prescription
(pat_id, med_id, pre_date, pre_dosage, pre_num_refills, pre_notes)

VALUES
(1, 5, '2014-03-25', 'Take one per day', '2', null),
(2, 4, '2012-09-21', 'Take two with a meal', '3', null),
(3, 3, '2016-07-09', 'Take one as needed', '1', null),
(4, 2, '2013-11-02', 'Take one per day', '1', null),
(5, 1, '2016-05-14', 'Take one twice daily', '2', null);

-------------------------------------------------------------------------------
--Data for table dbo.physician
-------------------------------------------------------------------------------
insert into dbo.physician
(phy_speciality, phy_fname, phy_lname, phy_street, phy_city, phy_state, phy_zip, phy_phone,
phy_fax, phy_email, phy_url, phy_notes)

VALUES
('Dermatology', 'Mary', 'Logan', '526 Main Street', 'Tampa', 'FL', '325420125', '9546523545',
'9542562501', 'dr_logan_derm@comcast.net', 'logandermatology.com', null),
('Psychiatrist', 'Derrick', 'Morgan', '454 Cherry Tree Drive', 'Jacksonville', 'FL', '322025402',
'9043344978', '9046733588', 'docmorgan', 'docmorgam.com', null),
('Gastroenterology', 'Camille', 'Strong', '1202 Ridenour Street', 'Miami', 'FL', '331795506', '7862368674',
'3057063381', 'cami_strong@gmail.com', 'stronggastro.com', null),
('Sports Medicine', 'Christopher', 'Mata', '3460 Grand Avenue', 'Orlando', 'FL', '328104722', '4077922946',
'3212297772', 'mata_sports@gmail.com', 'sportsmedicinemata.com', null),
('Orthopedic Surgeon', 'Luis', 'Walker', '3377 Woodside Circle', 'Panama City', 'FL', '324015426',
'8508721915', '5619459260', 'docwalker@comcast.net', 'walkerorthopedics.com', null);

-------------------------------------------------------------------------------
--Data for table dbo.treatment
-------------------------------------------------------------------------------
insert into dbo.treatment
(trt_name, trt_price, trt_notes)

values
('Microdermabrasion', 3500.00, null),
('Strep throat', 1250.00, null),
('Endoscopy', 12000.00, null),
('Knee replacemnt', 25000.00, null),
('Tonsil removal', 7500.00, null);

-------------------------------------------------------------------------------
--Data for table dbo.patient_treatment
-------------------------------------------------------------------------------
insert into dbo.patient_treatment
(pat_id, phy_id, trt_id, ptr_date, ptr_start, ptr_end, ptr_results, ptr_notes)

values
(1, 2, 3, '2014-05-12', '08:33:05', '09:53:52', 'Success patient recovering fine', null),
(2, 3, 4, '2015-09-22', '10:25:28', '12:43:05', 'Complications monitor patient', null),
(3, 4, 5, '2017-12-06', '15:48:31', '17:32:29', 'Died during surgery', null),
(4, 5, 1, '2013-01-15', '11:38:45', '13:24:35', 'Removal of only left side', null),
(5, 1, 2, '2018-07-30', '13:09:02', '14:49:44', 'Success patient recovering fine', null);

-------------------------------------------------------------------------------
--Data for table dbo.administration_lu
-------------------------------------------------------------------------------
insert into dbo.administration_lu
(pre_id, ptr_id)

VALUES
(1, 2),
(2, 3),
(3, 4),
(4, 5),
(5, 1);

--enable all constraints
EXEC sp_msforeachtable "alter table ? with check check constraint all"

/*
NOTE: both CHECKs needed:
1) First CHECK belongs with WITH
(ensures data gets checked for consistency when activating constraint)

2) second CHECK with CONSTRAINT
(type of constraint)
*/

--show data
select * from dbo.patient;
select * from dbo.medication;
select * from dbo.prescription;
select * from dbo.physician;
select * from dbo.treatment;
select * from dbo.patient_treatment;
select * from dbo.administration_lu;

/*a. Using JOIN ON, create a transaction that lists all patients’ first, last names, any
patients’ notes, as well as all of their medication names, prices, shelf lives, prescription
dosages and number of refills., order by medicine price in descending order.*/
--Note: By default, MySQL runs with autocommit mode enabled.
--Statements that modify data are immediately written to disk. Changes cannot be rolled back.
--START TRANSACTION disables autocommit until end of transaction with COMMIT or ROLLBACK.
--autocommit mode then reverts to its previous state.

/*START TRANSACTION;
    select pat_fname, pat_lname, pat_notes, med_name, med_price, med_shelf_life, pre_dosage,
    pre_num_refills
    from medication m 
        join prescription pr on pr.med_id = m.med_id
        join patient p on pr.pat_id = p.pat_id
    order by  med_price desc;
commit;

--with formatting
--LPAD(str, len, padstr); -- left pads a string with another string
--str (actual string)
--len (number indicating total length of string, in characters, returned after padding)
--padstr (string which used for left padding), below is padded with space character
/*Example: LPAD(med_price,7,''); -- total length of string = 7, that is, 5 decimal(5,2) +
1(decimal point) + 1(dollar sign)*/

--w//o lpad():
/*start TRANSACTION;
    select pat_fname, pat_lname, pat_notes, med_name, concat('$', format(med_price,2)) as price,
    med_shelf_life, pre_dosage, pre_num_refills
    from medication m 
        join prescription pr on pr.med_id = m.med_id
        join patient p on pr.pat_id = p.pat_id
    order by med_price desc;
commit;

--w/lpad():
start TRANSACTION;
    select pat_fname, pat_lname, pat_notes, med_name, lpad(concat('$', format(med_price,2)), 7, '')
    as price, med_shelf_life, pre_dosage, pre_num_refills
    from medication m 
        join prescription pr on pr.med_id = m.med_id
        join patient p on pr.pat_id = p.pat_id
    order by med_price desc;
commit;*/

-----------------------MS SQL Server-----------------------
use rnr06;
GO

begin TRANSACTION;
    select pat_fname, pat_lname, pat_notes, med_name, med_price, med_shelf_life, pre_dosage,
    pre_num_refills
    from medication m 
        join prescription pr on pr.med_id = m.med_id
        join patient p on pr.pat_id = p.pat_id
    order by med_price desc;
commit;



/*b. Using old-style join, create a view (dbo.v_physician_patient_treatments)that lists all
physicians’ first, last names, treatment names, treatment prices, treatment results, as well
as start and end dates, order by treatment price in descending order.*/
--old-style join
drop view if exists v_physician_patient_treatments;
create view v_physician_patient_treatments AS
/*with formatting: total length of string = 11, that is, 8 decimal(8,2) + 1(decimal point) +
1(dollar sign) + 1(comma -thousand separator)*/
select phy_fname, phy_lname, trt_name, lpad(concat('$', format(trt_price,2)), 11, '') AS
price, ptr_results, ptr_date
from physician p, patient_treatment pt, treatment t 
    where p.phy_id=pt.phy_id
        and pt.trt_id=t.trt_id
    order by trt_price desc;

--join using
drop view if exists v_physician_patient_treatments;
create view v_physician_patient_treatments AS
    select phy_fname, phy_lname, trt_name, trt_price, ptr_results, ptr_date
    from physician
        join patient_treatment using (phy_id)
        join treatment using (trt_id)
    order by trt_price desc;

select * from v_physician_patient_treatments;


-----------------------MS SQL Server-----------------------
use rnr06;
GO

--drop view if exists
--1st arg is object name, 2nd arg is trpe (V=view)
if object_id (N'dbo.v_physician_patient_treatments', N'V') is not NULL
drop view dbo.v_physician_patient_treatments;
GO

create view dbo.v_physician_patient_treatments as
    select phy_fname, phy_lname, trt_name, trt_price, ptr_results, ptr_date, ptr_start, ptr_end
    from physician p, patient_treatment pt, treatment t 
    where p.phy_id=pt.phy_id
        and pt.trt_id=t.trt_id;
GO

select * from dbo.v_physician_patient_treatments order by trt_price desc;
GO

select * from information_schema.tables;
GO

--show view definition
select view_definition
from information_schema.views;
GO



/*c. Create a stored procedure (AddRecord) that adds the following record to the patient
treatment table, and that uses the view above after the added record: patient id 5, physician
id 5, treatment id 5, 4/23/13, 11am, 12:30p, released, ok*/
use rnr06;
GO

--select before modifying database
select * from dbo.v_physician_patient_treatments;

if object_id('AddRecord') is not NULL
    drop procedure AddRecord;
GO

create procedure AddRecord AS
    insert into dbo.patient_treatment
    (pat_id, phy_id, trt_id, ptr_date, ptr_start, ptr_end, ptr_results, ptr_notes)
    values (5, 5, 5, '2013-04-23', '11:00:00', '12:30:00', 'released', 'ok');

select * from dbo.v_physician_patient_treatments;
GO

exec AddRecord;

--or... (here, outside)
--select after modifying database
select * from dbo.v_physician_patient_treatments;

drop PROCEDURE AddRecord;



/*d. Create a transaction that removes *only* the fifth record in the administration lookup
table—remember: it is a composite key!*/
--show data before/after
begin TRANSACTION;
    select * from dbo.administration_lu;
    delete from dbo.administration_lu where pre_id = 5 and ptr_id = 10;
    select * from dbo.administration_lu;
commit;



/*e. Create a stored procedure (UpdateRecord) to update the patient’s last name to
“Vanderbilt” whose id is 3.*/
use rnr06;
GO

if object_id('dbo.UpdatePatient') is not NULL
    drop procedure dbo.UpdatePatient;
GO

create procedure dbo.UpdatePatient AS

    select * from dbo.patient;

    update dbo.patient
    set pat_lname = 'Vanderbilt'
    where pat_id = 3;

    select * from dbo.patient;

GO

exec dbo.UpdatePatient;
GO

drop procedure dbo.UpdatePatient;
GO



/*f. Use an alter statement to create a “prognosis” attribute in the patient treatment table
capable of storing 255 alpha-numeric characters, just above the “notes” attribute. The
attribute value can be null.*/
exec sp_help 'dbo.patient_treatment';
alter table dbo.patient_treatment add ptr_prognosis varchar(255) null default 'testing';
exec sp_help 'dbo.patient_treatment';



--EXTRA CREDIT
use rnr06;
go

if object_id('dbo.AddShowRecords') is not NULL
    drop PROCEDURE dbo.AddShowRecords;
GO

create PROCEDURE dbo.AddShowRecords AS

    select * from dbo.patient;

    insert into dbo.patient
    (pat_ssn, pat_fname, pat_lname, pat_street, pat_city, pat_state, pat_zip, pat_phone, pat_email,
    pat_dob, pat_gender, pat_notes)
    values (756889432, 'John', 'Doe', '123 Main St', 'Tallahassee', 'FL', '32405', '8507863241',
    'jdoe@fsu.edu', '1999-005-10', 'M', 'testing notes');

    select * from dbo.patient;

    select phy_fname, phy_lname, pat_fname, pat_lname, trt_name, ptr_start, ptr_end, ptr_date
    from dbo.patient p 
        join dbo.patient_treatment pt on p.pat_id=pt.pat_id
        join dbo.physician pn on pn.phy_id=pt.phy_id
        join dbo.treatment t on t.trt_id=pt.trt_id
    order by ptr_date desc;
GO

exec dbo.AddShowRecords;
GO

drop procedure dbo.AddShowRecords;
GO
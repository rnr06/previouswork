--A5 1 ANSWER
--A5 MS SQL Server
--(not the same, but) similar to SHOW WARNINGS;
set ansi_warnings on;
GO

use master;
go

--drop existing database if it exists (use *your* username)
if exists (select name from master.dbo.sysdatabases where name = N'rnr06')
drop database rnr06;
GO

--create database if not exists (use *your* username)
if not exists (select name from master.dbo.sysdatabases where name = N'rnr06')
create database rnr06;
GO

use rnr06;
GO

-------------------------------------------------------------------------------
--Table dbo.applicant
-------------------------------------------------------------------------------
--drop table if exists
--N=subsequent string may be in Unicode (makes it portable to use with Unicode characters)
--U=only look for objects with this name that are tables
--*be sure* to use dbo. before *all* table references
if object_id
(N'dbo.applicant', N'U') is not NULL
drop table dbo.applicant;
GO

create table dbo.applicant
(
    app_id smallint not null identity(1,1),
    app_ssn int not null check (app_ssn > 0 and app_ssn <= 999999999),
    app_state_id varchar(45) not null,
    app_fname varchar(15) not null,
    app_lname varchar(30) not null,
    app_street varchar(30) not null,
    app_city varchar(30) not null,
    app_state char(2) not null default 'FL',
    app_zip int not null check (app_zip > 0 and app_zip <= 999999999),
    app_email varchar(100) null,
    app_dob date not null,
    app_gender char(1) not null check (app_gender IN('M', 'F')),
    app_bckgd_check char(1) not null check (app_bckgd_check IN('N', 'Y')),
    app_notes varchar(45) null,
    primary key (app_id),

    --make sure SSNs and State IDs are unique
    constraint ux_app_ssn unique nonclustered (app_ssn ASC),
    constraint ux_app_state_id unique nonclustered (app_state_id ASC)
);

--non clustered by default

--END A5 1 ANSWER



--A5 2 ANSWER
-------------------------------------------------------------------------------
--Table dbo.property
-------------------------------------------------------------------------------
if object_id  (N'dbo.property', N'U') is not NULL
drop table dbo.property;

create table dbo.property
(
    prp_id smallint not null identity(1,1),
    prp_street varchar(30) not null,
    prp_city varchar(30) not null,
    prp_state char(2) not null default 'FL',
    prp_zip int not null check (prp_zip > 0 and prp_zip <= 999999999),
    prp_type varchar(15) not null check
        (prp_type IN('House', 'Condo', 'Townhouse', 'Duplex', 'Apt', 'Mobile Home', 'Room')),
    prp_rental_rate decimal(7,2) not null check (prp_rental_rate > 0),
    prp_status char(1) not null check (prp_status IN('A', 'U')),
    prp_notes varchar(255) null,
    primary key (prp_id)
);

-------------------------------------------------------------------------------
--Table dbo.agreement
-------------------------------------------------------------------------------
if object_id (N'dbo.agreement', N'U') is not null
drop table dbo.agreement;

create table dbo.agreement
(
    agr_id smallint not null identity(1,1),
    prp_id smallint not null,
    app_id smallint not null,
    agr_signed date not null,
    agr_start date not null,
    agr_end date not null,
    agr_amt decimal(7,2) not null check (agr_amt > 0),
    agr_notes varchar(255) null,
    primary key (agr_id),

    --make sure combination of prp_id, app_id, and agr_signed is unique
    constraint ux_prp_id_app_id_agr_signed unique NONCLUSTERED
    (prp_id ASC, app_id ASC, agr_signed ASC),

    constraint fk_agreement_property
        foreign key (prp_id)
        references dbo.property (prp_id)
        on delete cascade
        on update cascade,
    
    constraint fk_agreement_applicant
        foreign key (app_id)
        references dbo.applicant (app_id)
        on delete CASCADE
        on update cascade
);

--END A5 2 ANSWER



--A5 3 ANSWER
-------------------------------------------------------------------------------
--Table dbo.feature
-------------------------------------------------------------------------------
if object_id (N'dbo.feature', N'U') is not NULL
drop table dbo.feature;

create table dbo.feature
(
    ftr_id tinyint not null identity(1,1),
    ftr_type varchar(45) not null,
    ftr_notes varchar(255) null,
    primary key (ftr_id)
);

-------------------------------------------------------------------------------
--Table dbo.prop_feature
-------------------------------------------------------------------------------
if object_id (N'dbo.prop_feature', N'U') is not NULL
drop table dbo.prop_feature;

create table dbo.prop_feature
(
    pft_id smallint not null identity(1,1),
    prp_id smallint not null,
    ftr_id tinyint not null,
    pft_notes varchar(255) null,
    primary key (pft_id),

    --make sure combination of prp_id and ftr_id is unique
    constraint ux_prop_id_ftr_id unique nonclustered (prp_id ASC, ftr_id ASC),

    constraint fk_prop_feat_property
        foreign key (prp_id)
        references dbo.property (prp_id)
        on delete CASCADE
        on update cascade,

    constraint fk_prop_feat_feature
        foreign key (ftr_id)
        references dbo.feature (ftr_id)
        on delete CASCADE
        on update cascade
);

-------------------------------------------------------------------------------
--Table dbo.occuant
-------------------------------------------------------------------------------
if object_id (N'dbo.occupant', N'U') is not NULL
drop table dbo.occupant;

create table dbo.occupant
(
    ocp_id smallint not null identity(1,1),
    app_id smallint not null,
    ocp_ssn int not null check (ocp_ssn > 0 and ocp_ssn <= 999999999),
    ocp_state_id varchar(45) null,
    ocp_fname varchar(15) not null,
    ocp_lname varchar(30) not null,
    ocp_email varchar(100) null,
    ocp_dob date not null,
    ocp_gender char(1) not null check (ocp_gender IN('M', 'F')),
    ocp_bckgd_check char(1) not null check (ocp_bckgd_check IN('N', 'Y')),
    ocp_notes varchar(45) null,
    primary key (ocp_id),

    --make sure SSNs and State IDs are unique
    constraint ux_ocp_ssn unique nonclustered (ocp_ssn ASC),
    constraint ux_ocp_state_id unique nonclustered (ocp_state_id ASC),

    constraint fk_occupant_applicant
        foreign key (app_id)
        references dbo.applicant (app_id)
        on delete CASCADE
        on update CASCADE
);

--END A5 3 ANSWER



--A5 4 ANSWER
-------------------------------------------------------------------------------
--Table dbo.phone
-------------------------------------------------------------------------------
if object_id (N'dbo.phone', N'U') is not NULL
drop table dbo.phone;
--Note: an applicant *must* provide a phone number, but can have null values, if phone(s) belong(s) exclusively to their occupants

create table dbo.phone
(
    phn_id smallint not null identity(1,1),
    app_id smallint null, --can be null, but *must* provide at least one number
    ocp_id smallint null,
    phn_num bigint not null check (phn_num > 0 and phn_num <= 9999999999),
    phn_type char(1) not null check (phn_type IN('C','H','W','F')),
    phn_notes varchar(255) null,
    primary key (phn_id),

    --make sure combination of app_id and phn_num is unique
    constraint ux_app_id_phn_num unique nonclustered (app_id ASC, phn_num ASC),

    --make sure combination of ocp_id and phn_num is unique
    constraint ux_ocp_id_phn_num unique nonclustered (ocp_id ASC, phn_num ASC),

    constraint fk_phone_applicant
        foreign key (app_id)
        references dbo.applicant (app_id)
        on delete CASCADE
        on update cascade,

    --ocp_id must be NO ACTION, because of the multiple paths with applicant (parent table)
    --Example: if applicant deleted, phone will be deleted, as will the occupant.
    --Allowing occupant to delete will attempt to delete the same record twice--same with updating!
    --ERROR! Introducing FOREIGN KEY constraint 'fk_phone_occupant' on table 'phone' may cause cycles or multiple cascade paths.
    --Specify ON DELETE NO ACTION or ON UpDATE NO ACTION,
    constraint fk_phone_occupant
        foreign key (ocp_id)
        references dbo.occupant (ocp_id)
        on delete no ACTION
        on update no action
);

-------------------------------------------------------------------------------
--Table dbo.room_type
-------------------------------------------------------------------------------
if object_id (N'dbo.room_type', N'U') is not NULL
drop table dbo.room_type;

create table dbo.room_type
(
    rtp_id tinyint not null identity(1,1),
    rtp_name varchar(45) not null,
    rtp_notes varchar(255) null,
    primary key (rtp_id)
);

--END A5 4 ANSWER



--A5 5 ANSWER
-------------------------------------------------------------------------------
--Table dbo.room
-------------------------------------------------------------------------------
if object_id (N'dbo.room', N'U') is not NULL
drop table dbo.room;

create table dbo.room
(
    rom_id smallint not null identity(1,1),
    prp_id smallint not null,
    rtp_id tinyint not null,
    rom_size varchar(45) not null,
    rom_notes varchar(255) null,
    primary key (rom_id),

    --can have duplicate room types in same property
    --make sure combination of prp_id and rtp_id in unique
    --CONSTRAINT ux_prp_id_rtp_id unique nonclustered (prp_id ASC, rtp_id ASC),

    CONSTRAINT fk_room_property
        foreign key (prp_id)
        references dbo.property (prp_id)
        on delete CASCADE
        on update cascade,

    constraint fk_room_type
        foreign key (rtp_id)
        references dbo.room_type (rtp_id)
        on delete CASCADE
        on update cascade
);

--show tables;
select * from information_schema.tables;

--disable all constraints (must do this *after* table creation, but *before* inserts)
exec sp_msforeachtable "alter table ? nocheck constraint all"

--END A5 5 ANSWER



--A5 6 ANSWER
--disable all constraints (must do this *after* table creation, but *before* inserts)
exec sp_msforeachtable "alter table ? nocheck constraint all"

-------------------------------------------------------------------------------
--Data for table dbo.feature
-------------------------------------------------------------------------------
insert into dbo.feature
(ftr_type, ftr_notes)

VALUES
('Central A/C', null),
('Pool', null),
('Close to school', null),
('Furnished', null),
('Cable', null),
('Washer/Dryer', null),
('Refrigerator', null),
('Microwave', null),
('Oven', null),
('1-car garage', null),
('2-car garage', null),
('Sprinkler system', null),
('Security', null),
('Wi-Fi', null),
('Storage', null),
('Fireplace', null);

-------------------------------------------------------------------------------
--Data for table dbo.room_type
-------------------------------------------------------------------------------
insert into dbo.room_type
(rtp_name, rtp_notes)

VALUES
('Bed', null),
('Bath', null),
('Kitchen', null),
('Lanai', null),
('Dining', null),
('Living', null),
('Basement', null),
('Office', null);

-------------------------------------------------------------------------------
--Data for table dbo.prop_feature
-------------------------------------------------------------------------------
insert into dbo.prop_feature
(prp_id, ftr_id, pft_notes)

VALUES
(1, 4, null),
(2, 5, null),
(3, 3, null),
(4, 2, null),
(5, 1, null),
(1, 1, null),
(1, 5, null);

--END A5 6 ANSWER



--A5 7 ANSWER
-------------------------------------------------------------------------------
--Data for table dbo.room (escape single quotation mark by doubling single quotation mark)
-------------------------------------------------------------------------------
insert into dbo.room
(prp_id, rtp_id, rom_size, rom_notes)

VALUES
(1,1, '10" x 10"', null),
(3,2, '20" x 15"', null),
(4,3, '8" x 8"', null),
(5,4, '50" x 50"', null),
(2,5, '30" x 30"', null);

-------------------------------------------------------------------------------
--Data for table dbo.property (will work with zip code entered as string value)
-------------------------------------------------------------------------------
insert into dbo.property
(prp_street, prp_city, prp_state, prp_zip, prp_type, prp_rental_rate, prp_status, prp_notes)

VALUES
('5133 3rd Road', 'Lake Worth', 'FL', '334671234', 'house', 1800.00, 'u', null),
('92E Blah Way', 'Tallahassee', 'FL', '323011234', 'apt', 641.00, 'u', null),
('756 Diet Coke Lane', 'Panama City', 'FL', '342001234', 'condo', 2400.00, 'a', null),
('574 Doritos Circle', 'Jacksonville', 'FL', '365231234', 'townhouse', 1942.99, 'a', null),
('2241 W. Pensacola Street', 'Tallahassee', 'FL', '323031234', 'apt', 610.00, 'u', null);

-------------------------------------------------------------------------------
--Data for table dbo.applicant
-------------------------------------------------------------------------------
insert into dbo.applicant
(app_ssn, app_state_id, app_fname, app_lname, app_street, app_city, app_state, app_zip, app_email,
app_dob, app_gender, app_bckgd_check, app_notes)

VALUES
('123456789', 'A12C34S56Q78', 'Carla', 'Vanderbilt', '5133 3rd Road', 'Lake Worth', 'FL', '334671234', 'csweeney@yahoo.com', '1961-11-26', 'F', 'Y', null),
('590123654', 'B123A456D789', 'Amanda', 'Lindell', '2241 W. Pensacola Street', 'Tallahassee', 'FL', '323031234', 'acc10c@my.fsu.edu', '1981-04-04', 'F', 'Y', null),
('987456321', 'dfed66532sedd', 'David', 'Stephens', '1293 Banana Code Drive', 'Panama City', 'FL', '323081234', 'mjowett@comcast.net', '1965-05-15', 'M', 'N', null),
('365214986', 'dgfgr56597224', 'Chris', 'Thrombough', '987 Learning Drive', 'Tallahassee', 'FL', '323011234', 'landbeck@fsu.edu', '1969-07-25', 'M', 'Y', null),
('326598236', 'yadayada4517', 'Spencer', 'Moore', '787 Tharpe Road', 'Tallahassee', 'FL', '323061234', 'spencer@my.fsu.edu', '1990-08-14', 'M', 'N', null);

-------------------------------------------------------------------------------
--Data for table dbo.agreement
-------------------------------------------------------------------------------
insert into dbo.agreement
(prp_id, app_id, agr_signed, agr_start, agr_end, agr_amt, agr_notes)

VALUES
(3, 4, '2011-12-01', '2012-01-01', '2012-12-31', 1000.00, null),
(1, 1, '1983-01-01', '1983-01-01', '1987-12-31', 800.00, null),
(4, 2, '1999-12-31', '2000-01-01', '2004-12-31', 1200.00, null),
(5, 3, '1999-07-31', '1999-08-01', '2004-07-31', 750.00, null),
(2, 5, '2011-01-01', '2011-01-01', '2013-12-31', 900.00, null);

-------------------------------------------------------------------------------
--Data for table dbo.occupant
-------------------------------------------------------------------------------
insert into dbo.occupant
(app_id, ocp_ssn, ocp_state_id, ocp_fname, ocp_lname, ocp_email, ocp_dob, ocp_gender, ocp_bckgd_check, ocp_notes)

VALUES
(1, '326532165', 'okd557ig4125', 'Bridget', 'Case-Sweeney', 'bcs10c@gmail.com', '1988-03-23', 'F', 'Y', null),
(1, '187452457', 'uhtooold', 'Brian', 'Sweeney', 'brian@sweeney.com', '1956-07-28', 'M', 'Y', null),
(2, '123456780', 'thisisdoggie', 'Skittles', 'McGoobs', 'skittles@wags.com', '2011-01-01', 'F', 'N', null),
(2, '098123664', 'thisiskitty', 'Smalls', 'Balls', 'smalls@meanie.com', '1988-03-05', 'M', 'N', null),
(5, '857694032', 'thisisbaby324', 'Baby', 'Girl', 'baby@girl.com', '2013-04-08', 'F', 'N', null);

--END A5 7 ANSWER



--A5 8 ANSWER
-------------------------------------------------------------------------------
--Data for table dbo.phone
-------------------------------------------------------------------------------
insert into dbo.phone
(app_id, ocp_id, phn_num, phn_type, phn_notes)
--note: unless indicated in notes, if both app_id and ocp_id it is a shared phone

VALUES
(1, null, '5615233044', 'H', null),
(2, null, '5616859976', 'F', null),
(5, 5, '8504569872', 'H', null),
(null, 1, '5613080898', 'C', 'occupant"s number only'), --note how to escape single quotation (w/another single quotation)
(3, null, '8504152365', 'W', null);

--enable all constraints
exec sp_msforeachtable "alter table ? with check check constraint all"

/*
NOTE: both CHECKs needed:
1) first CHECK belongs with WITH
(ensures data gets checked for consistency when activating constraint)

2) second CHECK with CONSTRAINT
(type of constraint)
*/

--show data
select * from dbo.feature;
select * from dbo.prop_feature;
select * from dbo.room_type;
select * from dbo.room;
select * from dbo.property;
select * from dbo.applicant;
select * from dbo.agreement;
select * from dbo.occupant;
select * from dbo.phone;

--END A5 8 ANSWER
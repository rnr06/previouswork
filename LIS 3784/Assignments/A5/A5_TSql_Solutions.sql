--DEMO TRANSACTIONS

--MySQL

start transaction;
--do something
commit;

--MS SQL Server
begin transaction;
--do something
commit;

--END DEMO TRANSACTIONS

--A. Create a transaction that deletes applicant #1.

--A. ANSWER
--*be sure* in correct database (also, *be sure* to review ERD)
use rnr06;
GO

begin transaction;
--test before/after when modifying data
    select * from dbo.property;
    select * from dbo.prop_feature;
    select * from dbo.room;
    select * from dbo.agreement;

    delete from dbo.property
    where prp_id = 1;

--or, on same line...
--delete from dbo.property where prp_id =1;

    select * from dbo.property;
    select * from dbo.prop_feature;
    select * from dbo.room;
    select * from dbo.agreement
commit;

--END A. ANSWER


--B. ANSWER
use rnr06;
GO

--drop view if exists
--1st arg is object name, 2nd arg is type (V=view)
if object_id (N'dbo.v_prop_info', N'V') is not NULL
drop view dbo.v_prop_info;
GO

create view dbo.v_prop_info AS
select p.prp_id, prp_type, prp_rental_rate, rtp_name, rom_size
from property p 
    join room r on p.prp_id = r.prp_id
    join room_type rt on r.rtp_id = rt.rtp_id
where p.prp_id=3;

/*
old-style join
select p.prp_id, prp_type, prp_rental_rate, rtp_name, rom_size
from property p, room r, room_type rt
where p.prp_id = r.prp_id
    and r.rtp_id = rt.rtp_id
    and p.prp_id = 3;
*/
GO

select * from information_schema.tables;
GO

--show view definition
select view_definition
from information_schema.views;
GO

select * from dbo.v_prop_info;
GO

--END B. ANSWER



--C. ANSWER
use rnr06;
go

--compare view solutions below (order by clauses)
--drop view if exists
--1st arg is object name, 2nd arg is type (V=view)
--1) using TOP and order by in view
if object_id (N'dbo.v_prop_info_feature', N'V') is not NULL
drop view dbo.v_prop_info_feature;
GO

--Summary: In MS SQL Server, it is not advisable to use ORDER BY in Views. Use ORDER BY outside the views.
--BEST PRACTICES SOLUTION!
if object_id (N'dbo.v_prop_info_feature', N'V') is not NULL
drop view dbo.v_prop_info_feature;
GO

create view dbo.v_prop_info_feature AS
--join on
    select p.prp_id, prp_type, prp_rental_rate, ftr_type
    from property p 
        join prop_feature pf on p.prp_id=pf.prp_id
        join feature f on pf.ftr_id=f.ftr_id
    where p.prp_id >= 4 and p.prp_id < 6;
GO

--use order by *outside* of view
select * from dbo.v_prop_info_feature order by prp_rental_rate desc;
GO

select * from information_schema.tables;
GO

--show view definition
select view_definition
from information_schema.views;
GO

select  * from dbo.v_prop_info_feature;
GO

--END C. ANSWER



--D. ANSWER
use rnr06;
go

if object_id('dbo.ApplicantInfo') is not NULL
    drop procedure dbo.ApplicantInfo;
GO

--Unlike MySQL, no need to change delimiter (go command interpreted as end of CREATE PROCEDURE statement)
create procedure dbo.ApplicantInfo(@appid INT) AS
--old-style join
    select app_ssn, app_state_id, app_fname, app_lname, phn_num, phn_type
    from applicant a, phone p 
    where a.app_id = p.app_id
    and a.app_id=@appid;
GO

/* or, JOIN ON
select app_ssn, app_state, app_fname, app_lname, phn_num, phn_type
from applicant a
    join phone p on a.app_id=p.app_id
    and a.app_id=@appid;
*/

exec dbo.ApplicantInfo 3;

--or...

declare @myVar int = 2
exec dbo.ApplicantInfo @myVar;

/*
declare @myVar int = 2
--displaying values of variables: use Select or Print statements:
Select @myVar
Print @myVar -- prints as message
*/

GO

drop procedure dbo.ApplicantInfo;
GO

--END D. ANSWER



--E. ANSWER
use rnr06;
GO

if object_id('dbo.OccupantInfo') is not NULL
    drop procedure dbo.OccupantInfo;
GO

--no need to change delimiter (go command interpreted as end of CREATE PROCEDURE statement)
create procedure dbo.OccupantInfo as
    --left outer join (displays *all* values in phone table, including those with unmatched values in occupant)
        select ocp_ssn, ocp_state_id, ocp_fname, ocp_lname, phn_num, phn_type
        from phone p 
            left outer join occupant o on o.ocp_id = p.ocp_id;
GO

exec dbo.OccupantInfo;
GO

drop procedure dbo.OccupantInfo;
GO

--right outer join (same return - displays *all* values in phone table, including those with unmatched values in occupant)
select ocp_ssn, ocp_state_id, ocp_fname, ocp_lname, phn_num, phn_type
from occupant o 
    right outer join phone p on o.ocp_id = p.ocp_id;

--END E. ANSWER
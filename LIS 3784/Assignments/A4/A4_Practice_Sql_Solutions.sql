--A4 Practice
--avoids error that user kept db connection open
use master;
GO

--drop existing database if it exists (use *your* username)
if exists (select name from master.dbo.sysdatabases where name = N'rnr06')
drop database rnr06;
GO

-- create database if not exists (use *your* username)
if not exists (select name from master.dbo.sysdatabases where name = N'rnr06')
create database rnr06;
GO

use rnr06;
GO

--drop table if exists
--N=subsequent string may be in Unicode (makes it portable to use with Unicode characters)
--U=only look forobjects with this name that are tables
--*be sure* to use dbo. before *all* table references
if object_id (N'dbo.slsrep', N'U') is not NULL
drop table dbo.slsrep;
GO

--create table (can't use unsigned)
create table dbo.slsrep
(
    srp_id smallint not null identity(1,1),
    srp_fname varchar(15) not null,
    srp_lname varchar(30) not null,
    srp_sex char(1) not null check (srp_sex IN('m', 'f')),
    srp_age tinyint not null check (srp_age >= 18 and srp_age <=70),
    srp_street varchar(30) not null,
    srp_city varchar(30) not null,
    srp_state char(2) not null default 'FL',
    srp_zip int not null check (srp_zip > 0 and srp_zip <= 999999999),
    --Or, to require entires in the zip column to be 9 digits, type:
    --srp_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
    srp_phone bigint not null,
    srp_email varchar(100) not null,
    srp_url varchar(100) not null,
    srp_tot_sales decimal(10,2) not null check (srp_tot_sales > 0),
    srp_comm decimal(3,2) not null check (srp_comm >= 0 and srp_comm <= 1.00),
    srp_notes varchar(255) null,
    --example to create unique constraint on SSN
    --constraint ux_srp_ssn unique(srp_ssn)
    primary key(srp_id)
);

select * from information_schema.tables;

--don't include identity (auto-increment column)
insert into dbo.slsrep
(srp_fname, srp_lname, srp_sex, srp_age, srp_street, srp_city, srp_state, srp_zip, srp_phone,
srp_email, srp_url, srp_tot_sales, srp_comm, srp_notes)
values
('John', 'Doe', 'm', 18, '123 Main', 'Tallahassee', 'FL', '999999999', '8503457621',
'jdoe@aol.com', 'www.johndoe.com', 1000.00, .10, 'testing'),
('Jane', 'Smith', 'f', 24, '395 Fleming Way', 'Richmond', 'VA', '232349999', '8047434928',
'jane_smith@gmail.com', 'www.janesmith.com', 1500.00, .10, ''),
('Madison', 'Hall', 'f', 27, '190 Eastland Avenue', 'Jackson', 'MS', '392119999', '6014274207',
'madisonhall@gmail.com', 'www.mhall.com', 1200.00, .08, ''),
('George', 'Glass', 'm', 30, '23 Fairfield Court', 'Metairie', 'LA', '700019999', '4796488075',
'gglass@comcast.net', 'www.georgeglass.com', 900.00, .07, ''),
('Savannah', 'Wilson', 'f', 24, '832 Woodrow Way', 'Huntsville', 'TX', '773409999', '9364388992',
'savywilson@gmail.com', 'www.savwilson.com', 1150.00, .09, '');

select * from dbo.slsrep;

--drop table if exists
if object_id (N'dbo.customer', N'U') is not NULL
drop table dbo.customer;
GO

--create table (can't use unsigned)
create table dbo.customer
(
    cus_id smallint not null identity(1,1),
    srp_id smallint null,
    cus_fname varchar(15) not null,
    cus_lname varchar(30) not null,
    cus_sex char(1) not null check (cus_sex IN('m', 'f')),
    cus_age tinyint not null check (cus_age >= 18 and cus_age <=120),
    cus_street varchar(30) not null,
    cus_city varchar(30) not null,
    cus_state char(2) not null default 'FL',
    cus_zip int not null check (cus_zip > 0 and cus_zip <= 999999999),
    --Or, to require entires in the zip column to be 9 digits, type:
    --cus_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
    cus_phone bigint not null,
    cus_email varchar(100) not null,
    cus_url varchar(100) not null,
    cus_balance decimal(7,2) not null check (cus_balance > 0),
    cus_tot_sales decimal(10,2) not null check (cus_tot_sales > 0),
    cus_notes varchar(255) null,
    primary key(cus_id),
    constraint fk_cus_slsrep
        foreign key (srp_id)
        references dbo.slsrep (srp_id)
        on delete CASCADE
        on update CASCADE
);

select * from information_schema.tables;

insert into dbo.customer
(srp_id, cus_fname, cus_lname, cus_sex, cus_age, cus_street, cus_city, cus_state, cus_zip, cus_phone,
cus_email, cus_url, cus_balance, cus_tot_sales, cus_notes)
values
(1, 'Kevin', 'Crane', 'm', 28, '64 Booter Lane', 'Charlotte', 'NC', '282175625', '7044405270',
'kcrane@aol.com', 'www.crane.com', 125.00, 550.00, 'testing'),
(2, 'Hannah', 'Noel', 'f', 31, '2906 Black Oak Hollow Road', 'San Jose', 'CA', '951340263', '4087547586',
'hnoel@gmail.com', 'www.hnoel.com', 850.00, 2500.00, ''),
(3, 'Oscar', 'Kee', 'm', 35, '3183 O Conner Street', 'Gulfport', 'MS', '395017025', '2289934339',
'oscarkee@gmail.com', 'www.oscarkee.com', 475.00, 1250.00, ''),
(4, 'Brian', 'Cook', 'm', 19, '160 Ash Street', 'Dallas', 'TX', '752070201', '9728272077',
'briancook@comcast.net', 'www.briancook.com', 250.00, 975.00, ''),
(5, 'Christina', 'Smith', 'f', 26, '4144 George Street', 'Dade City', 'FL', '335258260', '3525187419',
'tinasmith@gmail.com', 'www.christinasmith.com', 450.00, 1675.00, '');

select * from dbo.customer;

--get table info, including foreign keys:
--EXEC sp_help 'tablename';
exec sp_help 'dbo.customer';



--F5 to run in MS Sql




insert into dbo.slsrep
(srp_fname, srp_lname, srp_sex, srp_age, srp_street, srp_city, srp_state, srp_zip, srp_phone,
srp_email, srp_url, srp_tot_sales, srp_comm, srp_notes)
values
('Jane', 'Smith', 'f', 24, '395 Fleming Way', 'Richmond', 'VA', '232349999', '8047434928',
'jane_smith@gmail.com', 'www.janesmith.com', 1500.00, .10, ''),
('Madison', 'Hall', 'f', 27, '190 Eastland Avenue', 'Jackson', 'MS', '392119999', '6014274207',
'madisonhall@gmail.com', 'www.mhall.com', 1200.00, .08, ''),
('George', 'Glass', 'm', 30, '23 Fairfield Court', 'Metairie', 'LA', '700019999', '4796488075',
'gglass@comcast.net', 'www.georgeglass.com', 900.00, .07, ''),
('Savannah', 'Wilson', 'f', 24, '832 Woodrow Way', 'Huntsville', 'TX', '773409999', '9364388992',
'savywilson@gmail.com', 'www.savwilson.com', 1150.00, .09, '');

select * from dbo.slsrep;



insert into dbo.customer
(srp_id, cus_fname, cus_lname, cus_sex, cus_age, cus_street, cus_city, cus_state, cus_zip, cus_phone,
cus_email, cus_url, cus_balance, cus_tot_sales, cus_notes)
values
(1, 'Kevin', 'Crane', 'm', 28, '64 Booter Lane', 'Charlotte', 'NC', '282175625', '7044405270',
'kcrane@aol.com', 'www.crane.com', 125.00, 550.00, 'testing'),
(2, 'Hannah', 'Noel', 'f', 31, '2906 Black Oak Hollow Road', 'San Jose', 'CA', '951340263', '4087547586',
'hnoel@gmail.com', 'www.hnoel.com', 850.00, 2500.00, ''),
(3, 'Oscar', 'Kee', 'm', 35, '3183 O Conner Street', 'Gulfport', 'MS', '395017025', '2289934339',
'oscarkee@gmail.com', 'www.oscarkee.com', 475.00, 1250.00, ''),
(4, 'Brian', 'Cook', 'm', 19, '160 Ash Street', 'Dallas', 'TX', '752070201', '9728272077',
'briancook@comcast.net', 'www.briancook.com', 250.00, 975.00, ''),
(5, 'Christina', 'Smith', 'f', 26, '4144 George Street', 'Dade City', 'FL', '335258260', '3525187419',
'tinasmith@gmail.com', 'www.christinasmith.com', 450.00, 1675.00, '');

select * from dbo.customer;
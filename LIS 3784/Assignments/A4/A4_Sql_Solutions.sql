--A4 Practice
--avoids error that user kept db connection open
use master;
GO

--drop existing database if it exists (use *your* username)
if exists (select name from master.dbo.sysdatabases where name = N'rnr06')
drop database rnr06;
GO

-- create database if not exists (use *your* username)
if not exists (select name from master.dbo.sysdatabases where name = N'rnr06')
create database rnr06;
GO

use rnr06;
GO

--drop table if exists
--N=subsequent string may be in Unicode (makes it portable to use with Unicode characters)
--U=only look forobjects with this name that are tables
--*be sure* to use dbo. before *all* table references
if object_id (N'dbo.petstore', N'U') is not NULL
drop table dbo.petstore;
GO

--create table (can't use unsigned)
create table dbo.petstore
(
    pst_id smallint not null identity(1,1),
    pst_name varchar(30) not null,
    pst_street varchar(30) not null,
    pst_city varchar(30) not null,
    pst_state char(2) not null default 'AZ',
    pst_zip int not null check (pst_zip > 0 and pst_zip <= 999999999),
    pst_phone bigint not null check (pst_phone > 0 and pst_phone <= 9999999999),
    pst_email varchar(100) not null,
    pst_url varchar(100) not null,
    pst_ytd_sales decimal(10,2) not null check (pst_ytd_sales > 0),
    pst_notes varchar(255) null,
    primary key(pst_id)
);

select * from information_schema.tables;

--don't include identity (auto-increment column)
insert into dbo.petstore
(pst_name, pst_street, pst_city, pst_state, pst_zip, pst_phone,
pst_email, pst_url, pst_ytd_sales, pst_notes)
values
('PetSmart', '123 Main Street', 'Tallahassee', 'FL', '32303', '8503457621',
'petsmart@petsmart.com', 'www.petsmart.com', 5255000.00, 'testing'),
('Petco', '2842 Washington Street', 'Corpus Christi', 'TX', '78476', '3614370705',
'petco@petco.com', 'www.petco.com', 4752500.00, ''),
('Devilish Paws', '1970  Lightning Point Drive', 'Memphis', 'TN', '38104', '9015166656',
'devilishpaws@gmail.com', 'www.devilishpaws.com', 1250000.00, ''),
('Your Feathery Friends', '4003 Kessla Way', 'Clinton', 'SC', '28328', '8433485568',
'featheryfriends@gmail.com', 'www.yourfeatheryfriends.com', 950000.00, ''),
('Squeaks and Squawks', '2568  Edington Drive', 'Symrna', 'GA', '30082', '6788274650',
'squeakssquawks@gmail.com', 'www.squeaksandsquawks.com', 1752500.00, '');

select * from dbo.petstore;

--drop table if exists
if object_id (N'dbo.pet', N'U') is not NULL
drop table dbo.pet;
GO

--create table (can't use unsigned)
create table dbo.pet
(
    pet_id smallint not null identity(1,1),
    pst_id smallint not null,
    pet_type varchar(45) not null,
    pet_sex char(1) not null check (pet_sex IN('M', 'F')),
    pet_cost decimal(6,2) not null check (pet_cost > 0),
    pet_price decimal(6,2) not null check (pet_price > 0),
    pet_age smallint not null check (pet_age > 0 and pet_age < 10500),
    pet_color varchar(30) not null,
    pet_sale_date date null,
    pet_vaccine char(1) not null check (pet_vaccine in('Y', 'N')),
    pet_neuter char(1) not null check (pet_neuter in('Y', 'N')),
    pet_notes varchar(255) null,
    primary key (pet_id),
    constraint fk_pet_petstore
        foreign key (pst_id)
        references dbo.petstore (pst_id)
        on delete CASCADE
        on update CASCADE
);

select * from information_schema.tables;

insert into dbo.pet
(pst_id, pet_type, pet_sex, pet_cost, pet_price, pet_age, pet_color, pet_sale_date, pet_vaccine,
pet_neuter, pet_notes)
values
(1, 'Dog', 'M', 350.00, 550.00, 1, 'Black','2009-04-13', 'Y', 'Y', ''),
(2, 'Dog', 'M', 850.00, 1000.00, 19, 'Harlequin', '2010-05-23', 'Y', 'N', 'testing1'),
(3, 'Cat', 'F', 600.00, 900.00, 12, 'White', '2014-08-06', 'N', 'Y', ''),
(4, 'Bird', 'M', 100.00, 150.00, 2, 'Yellow', '2016-06-25', 'Y', 'N', ''),
(5, 'Hampster', 'F', 25.00, 50.00, 3, 'Brown', '2012-10-10', 'Y', 'Y', '');

select * from dbo.pet;











--don't include identity (auto-increment column)
insert into dbo.petstore
(pst_name, pst_street, pst_city, pst_state, pst_zip, pst_phone,
pst_email, pst_url, pst_ytd_sales, pst_notes)
values
('PetSmart', '123 Main', 'Tallahassee', 'FL', '999999999', '8503457621',
'petsmart@petsmart.com', 'www.petsmart.com', 525500000.00, 'testing'),
('Petco', '123 Main', 'Clearwater', 'FL', '9999999999', '7045264502', 'petco@petco.com', 'www.petco.com',
475250000.00, 'testing');

select * from dbo.petstore;

insert into dbo.pet
(pst_id, pet_type, pet_sex, pet_cost, pet_price, pet_age, pet_color, pet_sale_date, pet_vaccine,
pet_neuter, pet_notes)
values
('1', 'Shih Tzu dog', 'M', 350.00, 550.00, '1', 'black','2009/04/13', 'Y', 'Y', 'testing');

select * from dbo.pet;








--get table info, including foreign keys:
--EXEC sp_help 'tabename';
exec sp_help 'dbo.customer';



--F5 to run in MS Sql








/*5. Create SQL queries to generate the following reports, and display their associated query
resultsets, copy from Secure Shell (PC) or Terminal (Mac):*/

/*a. Create a view that only displays users' ids, first and last names, phone numbers and e-mail
addresses, sort in ascending order by user last name (name it v_user_info):*/
drop view if exists v_user_info;
create view v_user_info as
    select usr_id, usr_fname, usr_lname,
    concat('(', substring(usr_phone,1,3), ')', substring(usr_phone,4,3), '-', substring(usr_phone,7,4))
    as usr_phone, usr_email
    from user
    order by usr_lname asc;

select * from v_user_info;



/*b. Create a view that only displays institutions' names, streets, cities, states, zip codes,
phone numbers and url addresses, display all address info. under one header, sort in ascending
order by institution name (name it v_institution_info):*/
drop view if exists v_institution_info;
create view v_institution_info as 
    select ins_name,
    concat(ins_street, '', '', ins_city, '', '', ins_state, '', '', substring(ins_zip,1,5), '-',
    substring(ins_zip,6,4)) as address,
    concat('(', substring(ins_phone,1,3), ')',substring(ins_phone,4,3), '-' substring(ins_phone,7,4))
    as ins_phone, ins_email, ins_url
    from institution
    order by ins_name asc;

select * from v_institution_info;



/*c. Create a view that only displays *unique* category types used (i.e., in transactions), not all
of the category types available (name it v_category_types):*/
select cat_type from category;

--displays ALL category types *available* in cat_type attribute (though, *NOT* answer)
describe category;

--NATURAL JOIN
drop view if exists v_category_types;
create view v_category_types AS
    select DISTINCT cat_id, cat_type
    from TRANSACTION
    natural join category;

select * from v_category_types;


--JOIN USING
drop view if exists v_category_types;
create view v_category_types AS
    select distinct cat_type
    from TRANSACTION
    join category using(cat_id);

select * from v_category_types;
--remove view from server memory
drop view if exists v_category_types;



/*d. Create a stored procedure that accepts a user’s id, and displays a user’s name, the institution
names, and the account types to which that user is associated, sort in descending order by account
type. (name it UserInstitutionAccountInfo):*/
drop procedure if exists UserInstitutionAccountInfo;
delimiter //
create procedure UserInstitutionAccountInfo(IN usrid INT)
BEGIN
    select usr_fname, usr_lname, ins_name, act_type
    from user u 
        join source s on u.usr_id=s.usr_id
        join institution i on s.ins_id=i.ins_id
        join account a on s.act_id=a.act_id
    where u.usr_id = usrid
    order by act_type desc;
end //
delimiter ;

--call procedure (see below)
set @uid=2;
call UserInstitutionAccountInfo(@uid);
--or...
call UserInstitutionAccountInfo(2);



/*e. Create a stored procedure that only displays users' names, account types, and transactions for
each account (include transaction type, method, amount, and date/time), (include date/time under one
header) for each user, sort in descending order by user last name, ascending order of amount (name
it UserAccountTransactionInfo):*/
drop procedure if exists UserAccountTransactionInfo;
delimiter //
create procedure UserAccountTransactionInfo()
BEGIN
    select usr_fname, usr_lname, act_type, trn_type, trn_method,
    concat('$', format(trn_amt, 2)) as trn_amount,
    date_format(trn_date, '%c%/%e%/%y %r') trn_timestamp,
    trn_notes
    from USER
        natural join source
        natural join TRANSACTION
        natural join account 
    order by usr_lname desc, trn_amt;
end //
delimiter ; --*be sure* there is a space between the semi-colon and DELIMITER!

--call procedure
call UserAccountTransactionInfo();



/*f. Create a view that only displays users' names and total of all debits, for each user, sort in
descending order by total debits (name it v_user_debits_info):*/
drop view if exists v_user_debits_info;
create view v_user_debits_info as
    select usr_fname, usr_lname, trn_type, concat('$',format(sum(trn_amt),2)) as debit_amt
    from USER
        natural join source
        natural join TRANSACTION
    where trn_type='debit'
    group by usr_id
    order by sum(trn_amt) desc;

select * from v_user_debits_info;



/*g. Create a transaction that inserts a record in the transaction table, updates it, and then
removes it.*/
start transaction;
    select * from transaction; --visible during transaction

    insert into TRANSACTION
    (trn_id, src_id, cat_id, trn_type, trn_method, trn_amt, trn_date, trn_notes)
    VALUES
    (NULL, 2, 1, 'credit', 'auto', 2235.09, '2002-01-07 11:59:59', NULL);

    select * from transaction; --visible during transaction

    --either way will work
    select @tid := max(trn_id) from transaction;

    --or...
    --set @tid = last_insert_id();

    update transaction;
    set trn_notes='transaction has been updated'
    where trn_id = @tid;

    select * from transaction; --visible during transaction

    delete from TRANSACTION
    where trn_id = @tid;

    select * from transaction; --visible during transaction

commit;



















use rnr06;
drop database if exists rnr06;
create database if not exists rnr06;
use rnr06;
source db.finance.sql
show tables;
describe account;
select * from account;



create view v_institution_info
    select ins_name, ins_street, ins_city, ins_state,
    concat(substring(ins_zip,1,5), '-', substring(ins_zip,6,4)) as ins_zip,
    concat('(, substring(ins_phone,1,3),')',substring(ins_phone,4,3), '-', substring(ins_phone,7,4)) as ins_phone, ins_email, ins_url
    from institution
    order by ins_name asc;
--i. List all faculty members’ first and last names, full addresses, salaries, and hire dates.
-- old style join
select p.per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date
from person as p, employee as e, faculty as f 
where p.per_id=e.per_id
and e.per_id=f.per_id;


OR


-- join on
select p.per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date
from person p 
join employee e on p.per_id=e.per_id
join faculty f on e.per_id=f.per_id;




OR


-- join using
select p.per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date
from person
join employee using (per_id)
join faculty using (per_id);



OR


-- natural join
select p.per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date
from person 
natural join employee
natural join faculty;

--HW answer: C and D



--ii. List the first 10 alumni’s names, genders, date of births, degree types, areas, and dates.
--old style join
select p.per_id, per_fname, per_lname, per_gender, per_dob, deg_type, deg_area, deg_date
from person p, alumnus a, degree d 
where p.per_id=a.per_id
and a.per_id=d.per_id
limit 0, 10;


--OR

--join on
select p.per_id, per_fname, per_lname, per_gender, per_dob, deg_type, deg_area, deg_date
from person p
join alumnus a on p.per_id=a.per_id
join degree d on a.per_id=d.per_id
limit 0, 10;


--OR


--join on
select p.per_id, per_fname, per_lname, per_gender, per_dob, deg_type, deg_area, deg_date
from person p
join alumnus a on p.per_id=a.per_id
join degree d on a.per_id=d.per_id
limit 10;


--OR


--join using
select p.per_id, per_fname, per_lname, per_gender, per_dob, deg_type, deg_area, deg_date
from person
join alumnus using(per_id)
join degree using(per_id)
limit 0, 10;


--OR


--natural join
select p.per_id, per_fname, per_lname, per_gender, per_dob, deg_type, deg_area, deg_date
from person
natural join alumnus
natural join degree
limit 0, 10;


--OR, last three alums in alphabetical order (EXTRA)
select p.per_id, per_fname, per_lname, per_gender, per_dob, deg_type, deg_area, deg_date
from person
natural join alumnus
natural join degree
order by per_lname desc
limit 0, 3;


--OR, last three alums in anumerical order (EXTRA)
select p.per_id, per_fname, per_lname, per_gender, per_dob, deg_type, deg_area, deg_date
from person
natural join alumnus
natural join degree
order by per_id desc
limit 0, 3;



--iii. List the last 20 undergraduate names, majors, tests, scores, and standings.
--old style join
select p.per_id, per_fname, per_lname, stu_major, ugd_test, ugd_score, ugd_standing
from person p, student s, undergrad u 
where p.per_id=s.per_id
and s.per_id=u.per_id 
order by per_id desc 
limit 0, 20;


--OR


--join on
select p.per_id, per_fname, per_lname, stu_major, ugd_test, ugd_score, ugd_standing
from person p 
join student s on p.per_id=s.per_id 
join undergrad u on s.per_id=u.per_id 
order by per_id desc 
limit 0, 20;


--OR


--join using
select per_id, per_fname, per_lname, stu_major, ugd_test, ugd_score, ugd_standing
from person
join student using (per_id) 
join undergrad using (per_id)
order by per_id desc 
limit 0, 20;



--OR


--natural join
select p.per_id, per_fname, per_lname, stu_major, ugd_test, ugd_score, ugd_standing
from person p 
natural join student 
natural join undergrad 
order by per_id desc 
limit 0, 20;





/*iv. Remove the first 10 staff members; after which, display the remaining staff members’
names and positions.*/
--check staff data
select * from staff;

--delete first 10 staff members
delete from staff
order by per_id limit 10;

--recheck staff data
select * from staff;



/*v. Update one graduate student’s test score (only one score) by 10%. Display the before
and after values to verify that it was updated.*/
--check grad student data (make sure update is not affecting *ALL* grad students!)
select * from grad;

/*here, "and" clause *NOT* needed. Only need "and"clause if per_id was part of a composite key,
and allowed more than one test*/
update grad
set grd_score=grd_score * 1.10
where per_id=75 and grd_test='gmat';

--recheck grad student data (make sure update is not affecting *ALL* grad students)
select * from grad;




/*vi. Add two new alumni, using only one SQL statement (include attributes). Then, verify
that two records have been added.*/
--check alumnus
select * from alumnus;

--using attribute names
insert into alumnus
(per_id, alm_notes)
values 
(97,"testing1"),
(98,"testing2");

select * from alumnus;

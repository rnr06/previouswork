/* i. List the members' first and last names, book ISBNs and titles, loan and due dates, and authors'
first and last names sorted in descending order of due dates.*/




/*ii. List an unstored derived attribute called "book sale price," that displays the current price,
which is marked down 15% from the original book price (format the number to two decimal places,
and include a dollar sign).*/




/*iii. Create a stored derived attribute based upon the calculation above for the second book in the
book table, and place the results in member #3's notes attribute.*/
select * from member;

--Step #1:
select bok_price, bok_price * .85
from book
where bok_isbn='1234567890234';

--Step #2:
select concat('Purchased book at discounted price: ', '$',format(bok_price * .85,2))
from book
where bok_isbn='123456789234';

--Step #3:
--check data
select * from member;

update member
set mem_notes =
    (
        select concat('Purchased book at discounted price: ', '$',format(bok_price * .85,2))
        from book
        where bok_isbn='123456789234'
    )
where mem_id = 3;

select * from member;



/*iv. Using only SQL, add a test table inside of your database with the following attribute
definitions (use prefix tst_ for each attribute), all should be not null except email, and notes:
id pk int unsigned AUTO INCREMENT,
fname varchar(15),
lname varchar(30),
street varchar(30),
city varchar(20),
state char(2),
zip int unsigned,
phone bigint unsigned COMMENT 'otherwise, cannot make contact',
email varchar(45) DEFAULT NULL,
notes varchar(255) DEFAULT NULL,
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci*/

drop table if exists test;
create table if not exists test
(
    tst_id int unsigned NOT NULL AUTO INCREMENT,
    tst_fname varchar(15) NOT NULL,
    tst_lname varchar(30) NOT NULL,
    tst_street varchar(30) NOT NULL,
    tst_city varchar(30) NOT NULL,
    tst_state char(2) NOT NULL default 'FL',
    tst_zip int unsigned NOT NULL,
    tst_phone bigint unsigned NOT NULL COMMENT 'otherwise, cannot make contact',
    tst_email varchar(100) DEFAULT NULL,
    tst_notes varchar(255) DEFAULT NULL,
    primary key (tst_id)
) engine=InnoDB default charset=utf8 collate utf8_general_ci;


--v. Insert data into test table from member table
select * from test;

insert into test
(tst_id, tst_fname, tst_lname, tst_street, tst_city, tst_state, tst_zip, tst_phone, tst_email,
tst_notes)
select mem_id, mem_fname, mem_lname, mem_street, mem_city, mem_state, mem_zip, mem_phone, mem_email,
mem_notes
from member;

select * from test;




/*vi. Alter the last name attribute in test table to the following options: tst_last varchar(35)
not null, default 'Doe' and comment "testing"*/
describe test;
alter table test change tst_lname tst_last varchar(35) not null default 'Doe' comment 'testing';
describe test;

show create table test;
show full columns from test;
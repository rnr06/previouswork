/************************************************************************************************
 * File Name:   InvoiceCalculator.java
 * 
 * Due Date:    March 26, 2018
 * 
 * Author:      Rhianna Reichert
 * 
 * Program Description:  
 *              This program will figure out what the discount rate is for each customer depending on their customer type and subtotal, then calculate their discount amount and the invoice total.  
 * 
 * Input:       The user will input two values: a numerical value (1-4) for their customer type and the subtotal.
 *
 * Output:      Print the invoice report: subtotal, customer type, discount percent, discount amount, and total to the screen.
 *              
 *
 * Processing:  Figure out the discount amount rate for each customer type and their total, then calculate the discount amount and invoice total. 
 *              
 *              
 * **********************************************************************************************/
 
import java.util.Scanner;
 
public class InvoiceCalculator
{
        public static void main(String[] args)
        {
            //Declare variables and Scanner object
            int cusType;        //Declaring the Customer Type as an Int
            double subtotal;    //Declaring the Subtotal as a Double
            double discPert = 0.0;    //Declaring the Discount Percentage as a Double
            double discAmt;     //Declaring the Discount Amount as a Double
            double invTotal;    //Declaring the Invoice Total as a Double
            
            Scanner keyboard = new Scanner(System.in);  //Creating the Scanner object called 'Keyboard' 

            //Display a application title to screen
            System.out.println("\n*****************************************************************************************************");
            System.out.println("\n\t\t\t\tSeminole Invoice Total Calculator");
            System.out.println("\n*****************************************************************************************************");

            //Prompt user for customer type
            System.out.print("\nPlease enter the customer type (Enter 1 for Bronze, 2 for Silver, 3 for Gold, or 4 for Platinum): ");

            //Read customer type
            cusType = keyboard.nextInt();

            //Prompt user for subtotal   
            System.out.print("\nPlease enter the subtotal: ");

            //Read subtotal
            subtotal = keyboard.nextDouble();

            //Calculate Discount Rate (using a control statement)
            switch (cusType)
            {
            
                case 1:
                    if (subtotal >= 1000){
                        discPert = 0.2;
                    }
                
                    else if (subtotal < 1000 && subtotal >= 500){
                        discPert = 0.15;
                    }
                
                    else if (subtotal < 500 && subtotal >= 300){
                        discPert = 0.1;
                    }
                
                    else if (subtotal < 300){
                        discPert = 0.05;
                    }
                    break;
                
                case 2:
                    discPert = 0.3;
                    break;
                
                case 3:
                    if (subtotal >= 5000){
                        discPert = 0.6;
                    }
                
                    else if (subtotal < 5000 && subtotal >= 2000){
                        discPert = 0.5;
                    }
                
                    else if (subtotal < 2000){
                        discPert = 0.5;
                    }
                    break;
                
                case 4:
                    discPert = 0.6;
                    break;
                    
                default:
                    if (subtotal >= 1000){
                        discPert = 0.2;
                    }
                
                    else if (subtotal < 1000 && subtotal >= 500){
                        discPert = 0.15;
                    }
                
                    else if (subtotal < 500 && subtotal >= 300){
                        discPert = 0.1;
                    }
                
                    else if (subtotal < 300){
                        discPert = 0.05;
                    }
                    break;
                    
            }
                

            //Calculate the Discount Amount
            discAmt = subtotal * discPert;

            //Calculate Invoice Total
            invTotal = subtotal - discAmt;

            //Format and display the results
            System.out.println("\n*****************************************************************************************************");
            System.out.println("\n\t\t\t\t\tINVOICE REPORT:\n");
            System.out.printf("\t\t\t\t\tSubtotal: %.2f \n" , subtotal);
            System.out.println("\t\t\t\t\tCustomer type: " + cusType);
            System.out.printf("\t\t\t\t\tDiscount percent: %.2f \n" , discPert);
            System.out.printf("\t\t\t\t\tDiscount amount: %.2f \n" , discAmt);
            System.out.printf("\t\t\t\t\tTotal: %.2f \n" , invTotal);
            
            //Display thank you message
            System.out.println();
            System.out.println("\t\t\t\t\tThank you!!");
            System.out.println("\n*****************************************************************************************************");


        }//end of main
}//end of InvoiceCalculator class
﻿/************************************************************************************************
 * File Name:   BankAccount.java
 * 
 * Due Date:    April 27, 2018
 * 
 * Author:      Rhianna Reichert
 * 
 * Program Description:  
 *              This program runs like a bank ATM. It prompts the user for their account number then asks for their menu choice and depending on their choice, will either ask for
 *              their deposit or withdrawl amount and give the user their new balance, give them their current balance, or exit the program.  
 * 
 * Input:       The user will input three values: a letter based on their menu choice, a five digit numerical value for their account number, and their deposit/withdrawl amount.  
 *
 * Output:      Prints the balance of the customers account to the screen.  
 *
 * Processing:  Figures out the correct menu option to display based on the user's input, calculates the balance after a deposit or calculates the balance after a withdrawl.  
 * **********************************************************************************************/

import java.util.Scanner;

public class BankAccount{
    

        //Main Function
        public static void main(String []args){
        
                //Declare Scanner object(s)
                Scanner input = new Scanner(System.in);  //Creating the Scanner object called 'input'
            
                //Declare other variables as needed
                //Declare and initialize a variable for balance to $5000.
                double balance = 5000.00;       //Declaring the account balance (balance) as a double and initializing it to $5000.00
                String menuChoice;              //Declaring the menu choice (choice) as a String
         
                //Display the welcome message -  (HINT:  This method does NOT require a value returned!)
                //Call the welcomeMessage() function
                welcomeMessage();

                //Prompt the user to enter the account number 
                //HINT:  The account number must be returned to main()!  In main(), don’t forget to assign the call statement to a variable.
                //Call the accountInfo() function
                int acctNum = accountInfo();
         
                //Process menu using a Do-While loop and Switch statement
                //This will Display the menu and Call the Function(s) based on the user's choice until the user enters X 

                do{
                        //Call the function to display the menu and prompt the user for their choice 
                        //HINT:  The displayMenu() function must return the choice back to main()!  In main(), don’t forget to assign the call statement to a variable.
                        //Call the displayMenu() function
                        menuChoice = displayMenu();
                    
                        //Start the switch() statement to determine which function is called based on the user’s choice
                        //HINT:  Each case calls or invokes a function to perform some specific task

                        switch(menuChoice){
                            
                                //Case (If the menu choice is D)
                                case "D":
                                        //Call the depositFunds(balance) function and assign it to a variable
                                        balance = depositFunds(balance);
                                        break;
                                        
                                //Case (If the menu choice is W)
                                case "W":
                                        //Call the withdrawFunds(balance) function and assign it to a variable
                                        balance = withdrawFunds(balance);
                                        break;
                                        
                                //Case (If the menu choice is B)
                                case "B":
                                        //Call the checkBalance(account number, balance) function
                                        checkBalance(acctNum, balance);
                                        break;
                                        
                                //Case (If the menu choice is X)
                                case "X":
                                        break;
                                
                                //Default for user error handling
                                default:
                                        //Display error message and prompt user for menu choice
                                        System.out.print("\nERROR: Please enter a D, W, B, or X: ");
                        
                                        //Read the user's menu choice and convert to uppercase
                                        menuChoice = input.next().toUpperCase();
                                
                                
                        }//end of switch     
             
                }while (!menuChoice.equals("X"));  
        
            
            //Display final message
            System.out.println("");
            System.out.println("Thank you for being a loyal Seminole Bank customer!");
            
        }//end of main
        
             
        /**************************************************** FUNCTION DEFINITIONS *****************************************************/

         //Display welcome message 
         public static void welcomeMessage(){
                
                //Display a application title to screen
                System.out.println("\n************************************************************************************************");
                System.out.println("\n\t\t\t\tWelcome to Seminole Bank!");
                System.out.println("\n************************************************************************************************");
             
         }//end of welcomeMessage
         
         //Prompt and Read users’ account number.  RETURN the account number to main().
         public static int accountInfo (){
                 
                //Declare LOCAL variables and Scanner object
                Scanner input = new Scanner(System.in);     //Creating the Scanner object called 'input'
                
                //Prompt the user for their account number
                System.out.println();
                System.out.print("Please enter your 5-digit Seminole Account Number: ");
        
                //Read the user's account number
                int acctNumber = input.nextInt();
                
                //Display thank you
                System.out.println("\nThank you!");
                
                //Return the user's account number
                return acctNumber;
             
         }//end of accountInfo
         
        
         //Display menu choices to the user and Read the users’ banking choice.  RETURN the user’s menu choice to main().
         public static String displayMenu (){
                 
                //Declare LOCAL variables and Scanner object
                String choice;                              //Declaring the menu choice (choice) as a String
                Scanner input = new Scanner(System.in);     //Creating the Scanner object called 'input'
                 
                //Promt the user for their menu choice
                System.out.print("\nEnter D for deposit, W for withdrawal, B for balance or X to exit the menu: ");
        
                //Read the user's menu choice and convert to uppercase
                choice = input.next().toUpperCase();
                
                //Return the user's menu choice
                return choice;
             
         }//end of displayMenu
         
         
         //Prompt the user for the amount to deposit and Read deposit amount.  Update the balance and RETURN the balance to main().
         public static double depositFunds(double balance){
                 
                //Declare LOCAL variables and Scanner object
                double deposit;                                 //Declaring the deposit amount (deposit) as a double
                Scanner input = new Scanner(System.in);         //Creating the Scanner object called 'input'
                
                //Prompt the user for the deposit amount
                System.out.print("\nEnter the amount of the deposit: ");
                        
                //Read the user's deposit amount
                deposit = input.nextDouble();
                
                //Calculate the current balance
                balance = balance + deposit;

                //Return the account balance after the deposit
                return balance;
                
         }//end of depositFunds
         
    
         //Prompt the user for the amount to withdraw and Read withdrawal amount.  Update the balance and RETURN the balance to main().
         public static double withdrawFunds (double balance){
                 
                //Declare LOCAL variables and Scanner object
                double withdrawal;                              //Declaring the withdrawal amount (withdrawal) as a double
                Scanner input = new Scanner(System.in);         //Creating the Scanner object called 'input'
                 
                //Prompt the user for the withdrawal amount
                System.out.print("\nEnter the amount of the withdrawal: ");
                
                //Read the user's withdrawl amount
                withdrawal = input.nextDouble();
                        
                //Calculate the current balance
                balance = balance - withdrawal;
                        
                //Return the account balance after the withdrawal
                return balance;
             
         }//end of withdrawFunds
     
         //Display the balance and DO NOT RETURN anything to main().
         public static void checkBalance(int acctNum, double balance){
                 
                //Display the current account balance
                System.out.printf("\nAccount number: " + acctNum + " has a current balance of: $%.2f" , balance);
                System.out.println("");
               
         }//end of checkBalance
                  
        /**************************************************END OF FUNCTION DEFINITIONS **************************************************/
    
    
}//end of BankAccount Class


/************************************************************************************************
 * File Name:   PhoneKeyPad.java
 * 
 * Due Date:    April 6, 2018
 * 
 * Author:      Rhianna Reichert
 * 
 * Program Description:  
 *              This program will ask the user to enter an uppercase letter then will figure out what the corresponding number for that letter is on a phone's key pad.
 * 
 * Input:       The user will input one character: an uppercase letter A-Z (letter).
 *
 * Output:      Print the corresponding number: number.
 *              
 *
 * Processing:  Figure out the correct number on a phone's key pad which corresponds to the letter which the user input. 
 *              
 *              
 * **********************************************************************************************/
 
import java.util.Scanner;

public class PhoneKeyPad {
    public static void main (String[] args) {
        
        //Declare variables and Scanner object
        char letter;    //Declaring the letter as a character
        int number;     //Declaring the number as an int
            
        Scanner input = new Scanner(System.in);  //Creating the Scanner object called 'input'
        
        //Display application title to screen
        System.out.println("\n*****************************************************************************************************");
        System.out.println("\n\t\t\t\tPhone Key Pad - Extra Credit");
        System.out.println("\n*****************************************************************************************************");
        
        //Prompt user for an uppercase letter
        System.out.println();
        System.out.print("Enter an uppercase letter: ");
        
        //Read uppercase letter
        letter = input.next().charAt(0);
        
        //Convert and store letter to uppercase
        letter = Character.toUpperCase(letter);
        
        switch (letter)
            {
            
                case 'A':
                    number = 2;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'B':
                    number = 2;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'C':
                    number = 2;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'D':
                    number = 3;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'E':
                    number = 3;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'F':
                    number = 3;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'G':
                    number = 4;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'H':
                    number = 4;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'I':
                    number = 4;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'J':
                    number = 5;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'K':
                    number = 5;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'L':
                    number = 5;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'M':
                    number = 6;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'N':
                    number = 6;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'O':
                    number = 6;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'P':
                    number = 7;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'Q':
                    number = 7;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'R':
                    number = 7;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'S':
                    number = 7;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'T':
                    number = 8;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'U':
                    number = 8;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'V':
                    number = 8;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'W':
                    number = 9;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'X':
                    number = 9;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'Y':
                    number = 9;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                case 'Z':
                    number = 9;
                        
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println("The corresponding number is " + number);
                    
                    break;
                    
                default:
                    //Display the statement for the corresponding number and the corresponding number
                    System.out.println("");
                    System.out.println(letter + " is an invalid input");
                    
                    break;
                    
            }
        
    } //end of main
    
} //end of the class
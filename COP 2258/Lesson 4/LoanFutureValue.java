/*************************************************************************
 * File Name: LoanFutureValue.java
 * 
 * Due Date:  Friday, February 23, 2018
 * 
 * Author:  Rhianna Reichert
 * 
 * Program Description:   This program will calculate the monthly interest rate and will calculate the future value.  
 * 
 * Input:       The user will input three values: investment amount, yearly interest rate, and the number of years. 
 *              
 * Processing:  Calculate the monthly interest rate and the future value. 
 * 
 * Output:      Print the monthly interest rate and the future value to thr screen.  
 * 
 * ***********************************************************************/

import java.util.*;

public class LoanFutureValue {
  public static void main(String[] args) {
    
    //Declare variables
    double investmentAmt;  //DECLARING THE DOUBLE INVESTMENT AMOUNT
    double yearInt;  //DECLARING THE DOUBLE YEARLY INTEREST
    int numYears;  //DECLARING THE INTEGER NUMBER OF YEARS
    double monthInt;  //DECLARING THE MONTHLY INTEREST
    double futureValue;  //DECLARING THE FUTURE VALUE
    double roundOff;  //DECLARING THE ROUND OFF
    
    //Create Scanner Object
    Scanner keyboard = new Scanner(System.in);

    // Step #1:  Display program header which includes the title
    System.out.println("\n*************************************************************************");
    System.out.println("\n              Accumulated Investment Value");
    System.out.println("\n*************************************************************************");
      
    // Step #2:  Prompt the user to enter the investment amount
    System.out.print("Enter the investment amount, for example 120000.95: ");
    
    // Step #3:  Read investment amount
    investmentAmt = keyboard.nextDouble();

    // Step #4:  Prompt the user to enter the yearly interest rate
    System.out.print("Enter annual interest rate, for example 8.25: ");
    
    // Step #5:  Read yearly interest rate
    yearInt = keyboard.nextDouble();

    // Step #6:  Prompt the user to enter the number of years
    System.out.print("Enter number of years as an integer, for example 5: ");
   
   // Step #7:  Read number of years 
   numYears = keyboard.nextInt();

    // Step #8:  Calculate monthly interest rate (yearly interest rate / 1200 = monthly interest rate)
    monthInt = yearInt / 1200;
      
    // Step #9:  Calculate the Future Value (investment amount * (1 + monthly innterest rate) ^ number of years * 12) and round the answer to two decimal places
    futureValue = investmentAmt * Math.pow(1 + monthInt, numYears * 12);
    roundOff = Math.round(futureValue * 100.0) / 100.0;
    
    // Step #10:  Display the future investment value
    System.out.println("\n*************************************************************************");
    System.out.println("\n              Future value is: " + roundOff);
    System.out.println("\n*************************************************************************");

  }//end of main method
  
}//end of LoanFutureValue class
/************************************************************************************************
 * File Name:   SeminoleBank_B.java
 * 
 * Due Date:    April 20, 2018
 * 
 * Author:      Rhianna Reichert
 * 
 * Program Description:  
 *              This program runs like a bank ATM. It prompts the user for their account number then asks for their menu choice and depending on their choice, will either ask for
 *              their deposit or withdrawl amount and give the user their new balance, give them their current balance, or exit the program.
 * 
 * Input:       The user will input three values: a letter based on their menu choice, a five digit numerical value for their account number, and their deposit/withdrawl amount.
 *
 * Output:      Prints the balance of the customers account to the screen.
 *              
 *
 * Processing:  Figures out the correct menu option to display based on the user's input, calculates the balance after a deposit or calculates the balance after a withdrawl. 
 *              
 *              
 * **********************************************************************************************/

import java.util.Scanner;

public class SeminoleBank_B {
    public static void main (String[] args) {
        
        //Declare variables and Scanner object
        int acctNum;                //Declaring the account number (acctNum) as an int
        String choice;              //Declaring the menu choice (choice) as a String
        double deposit;             //Declaring the amount of the deposit as a double
        double withdrawal;          //Declaring the amount of the withdrawal as a double
        double balance = 1000.00;   //Declaring the account balance (balance) as a double
        
        Scanner input = new Scanner(System.in);  //Creating the Scanner object called 'input'
        
        //Display a application title to screen
        System.out.println("\n*****************************************************************************************************");
        System.out.println("\n\t\t\t\tWelcome to Seminole Bank!");
        System.out.println("\n*****************************************************************************************************");
        
        //Prompt the user for their account number
        System.out.print("Please enter your 5-digit Seminole Account Number: ");
        
        //Read the user's account number
        acctNum = input.nextInt();
        
        //Display thank you
        System.out.println("\nThank you!");
        
        //Begin the Do-While loop
        do
        {
            //Promt the user for their menu choice
            System.out.print("\nEnter D for deposit, W for withdrawal, B for balance or X to exit the menu: ");
            
            //Read the user's menu choice and convert to uppercase
            choice = input.next().toUpperCase();
            
            //Start the switch statement
            switch (choice)
            {
                case "D":
                    {
                        //Prompt the user for the deposit amount
                        System.out.print("\nEnter the amount of the deposit: ");
                        
                        //Read the user's deposit amount
                        deposit = input.nextDouble();
                        
                        //Calculate the current balance
                        balance = balance + deposit;
                        
                        //Calculate the balance after the deposit
                        System.out.printf("\nAccount number: " + acctNum + " has a current balance of: $%.2f" , balance);
                        System.out.println("");
                        
                        break;
                    }
                    
                case "W":
                    {
                        //Prompt the user for the withdrawal amount
                        System.out.print("\nEnter the amount of the withdrawal: ");
                        
                        //Read the user's withdrawal amount
                        withdrawal = input.nextDouble();
                        
                        //Calculate the current balance
                        balance = balance - withdrawal;
                        
                        //Calculate the balance after the withdrawal
                        System.out.printf("\nAccount number: " + acctNum + " has a current balance of: $%.2f" , balance);
                        System.out.println("");
                        
                        break;
                    }
                    
                case "B":
                    {
                        //Display the current account balance
                        System.out.printf("\nAccount number: " + acctNum + " has a current balance of: $%.2f" , balance);
                        System.out.println("");
                        
                        break;
                    }
                    
                case "X":
                    {
                        break;
                    }
                    
                default:
                    {
                        //Display error message and prompt user for menu choice
                        System.out.print("\nERROR: Please enter a D, W, B, or X: ");
                        
                        //Read the user's menu choice and convert to uppercase
                        choice = input.next().toUpperCase();
                    }
            } //End of switch statement
            
        } //End of Do-While loop
        
        //While statement
        while ( !choice.equals("X"));
        
        //Display final message
        System.out.println("");
        System.out.println("Thank you for being a loyal Seminole Bank customer!");
        
    } //end of main
    
} //end of the class
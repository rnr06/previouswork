import java.util.Scanner;

public class AreaGame {
    public static void main (String[] args) {
        
        //Declare variables
        int width;
        int height;
        int area;
        int playValue;
        
        //Object to read input from the user
        Scanner keyboard = new Scanner (System.in);
        
        //Display a application title to screen
        System.out.println("\n*****************************************************************************************************");
        System.out.println("\n\t\t\t\t\tWelcome to the Area Game!");
        System.out.println("\n*****************************************************************************************************");
        
        //Start the Do/While Loop
        do {
            
            //Ask for the width and height from the user
            System.out.print("\nPlease enter the width of the rectangle: ");
            width = keyboard.nextInt();
        
            System.out.print("Please enter the height of the rectangle: ");
            height = keyboard.nextInt();
        
            //Calculate the area
            area = width * height;
        
            //Print the results
            System.out.println ("\nThe area of the rectangle is: " + area + "\n");
            
            //Ask the user if they want to continue playing the game
            System.out.print("Do you want to play again? (Type 1 to continue playing and 0 to stop playing): ");
        
            //Read the user's response
            playValue = keyboard.nextInt();
            
            //Verify that the user entered a 0 or a 1
            while (playValue != 0 && playValue != 1) {
                
                //User did not enter a 0 or a 1. Ask them to try again
                System.out.print("\nYou did not enter a correct value! Please enter 1 to continue playing or 0 to end the game: ");
                playValue = keyboard.nextInt();
                
            } //End of nested while
            
            //If the user entered 1, repeat steps again
            } while (playValue == 1);
            
            //Thank the user for playing
            System.out.println("\n*****************************************************************************************************");
            System.out.println("\n\t\t\tThank you for playing the Area Game! Goodbye!");
            System.out.println("\n*****************************************************************************************************");
            
    } //end of main
    
} //end of the class
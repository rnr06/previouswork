/************************************************************************************************
 * File Name:   SlackerRadio.java
 * 
 * Due Date:    <ENTER THE DATE>
 * 
 * Author:      Rhianna Reichert
 * 
 * Program Description:  
 *              <ENTER A DESCRIPTION>  
 * 
 * Input:       <ENTER THE INPUT REQUIREMENTS>
 *
 * Output:      <ENTER THE OUTPUT REQUIREMENTS> *              
 *
 * Processing:  <ENTER THE PROCESSING REQUIREMENTS> *              
 *              
 * **********************************************************************************************/

import java.util.Scanner; //needed to use Scanner for input

public class SlackerRadio {
  public static void main(String[] args) {
   
        //Declare variables
        String lname = "";
        int menuNum;
        String newStation = "";
        int stationNum;
        String stationMenu = "";
     
        //Create a Scanner object
        Scanner keyboard = new Scanner(System.in);  //Creating the Scanner object called 'keyboard'
    
        //Display the Opening Statement which includes the Slacker Radio Menu
        System.out.println("\n**************************************Welcome to Slacker Radio!**************************************");
        System.out.print("\n\t\t\t\tSLACKER RADIO MENU:");
        System.out.print("\n\t\t\t\t1 - Create a New Slacker Radio Station");
        System.out.print("\n\t\t\t\t2 - Play a Slacker Radio Station");
        System.out.println("\n\t\t\t\t3 - Exit Slacker Radio");
        System.out.println("\n*****************************************************************************************************");
        System.out.println("");
  
        //Prompt the user for their last name and menu choice option
        System.out.print("Please enter your last name followed by your Slacker Radio Menu Choice: ");
 
        //Read the user's lastname and read the user's menu choice; Parse string if necessary!
        lname = keyboard.next().toUpperCase();
        menuNum = keyboard.nextInt();
        
        //Convert last name to uppercase, if necessary
        lname = lname.toUpperCase();    //Storing to memory lname in ALL caps

        //Use a control statement such as an (if()/else if() or switch()) to process the user's menu choice (options:  1, 2, 3, other)
        switch (menuNum)
            {
            
                case 1:
                    //Create case 1 if user enters menu item 1
                    if (menuNum == 1)
                    {
                        //Display the statement for menu option 1
                        System.out.println("");
                        System.out.println("You have selected the CREATE A NEW Slacker Radio STATION menu option.");
                        System.out.println("");
                    
                        //Prompt the user for the name of their new radio station
                        System.out.print("Please enter the name of the new station: ");
                    
                        //Read the user's new radio station name
                        newStation = keyboard.nextLine();
                    
                        //Display the statement with the name of the new radio station
                        System.out.println("");
                        System.out.println("You have successfully created the following station: " + newStation.toUpperCase() );
                        System.out.println("");
                    }
                    
                    break;
                
                case 2:
                    //Create case 2 if user enters menu item 2
                    if (menuNum == 2)
                    {
                        //Display the statement for menu option 2
                        System.out.println("");
                        System.out.println("You have selected the PLAY A SLACKER RADIO STATION menu option.");
                        System.out.println("");
                        
                        //Display the statement listing all of the current created stations
                        System.out.println("The user " + lname.toUpperCase () + " currently has created the following stations:");
                        System.out.println("");
                        System.out.println("1. \tKenny Chesney");
                        System.out.println("2. \tCole Swindell");
                        System.out.println("3. \tMiranda Lambert");
                        System.out.println("4. \tLANCO");
                        System.out.println("5. \tOld Dominion");
                        System.out.println("6. \tPistol Annies");
                        System.out.println("7. \tDierks Bentley");
                        System.out.println("8. \tCarly Pearce");
                        System.out.println("9. \tBilly Currington");
                        System.out.println("10. \tEric Church");
                        System.out.println("");
                    
                        //Prompt the user for which radio station they would like to listen to
                        System.out.print("Which station would you like to listen to? (Enter 1, 2, 3, 4, 5, 6, 7, 8, 9, 10): ");
                    
                        //Read the user's selected radio station
                        stationNum = keyboard.nextInt();
                        
                        //If/ Else statement for displaying the correst station name depending on the user's input
                        if (stationNum == 1)
                        {
                            //Set the variable to the correct artist
                            stationMenu = "Kenny Chesney";
                            
                            //Display the statement for menu option 1 and convert the artist name to all uppercase
                            System.out.println("");
                            System.out.println("You are now listening to " + stationMenu.toUpperCase() + "!");
                        }
                        
                        else if (stationNum == 2)
                        {
                            //Set the variable to the correct artist
                            stationMenu = "Cole Swindell";
                            
                            //Display the statement for menu option 2 and convert the artist name to all uppercase
                            System.out.println("");
                            System.out.println("You are now listening to " + stationMenu.toUpperCase() + "!");
                        }
                        
                        else if (stationNum == 3)
                        {
                            //Set the variable to the correct artist
                            stationMenu = "Miranda Lambert";
                            
                            //Display the statement for menu option 3 and convert the artist name to all uppercase
                            System.out.println("");
                            System.out.println("You are now listening to " + stationMenu.toUpperCase() + "!");
                        }
                        
                        else if (stationNum == 4)
                        {
                            //Set the variable to the correct artist
                            stationMenu = "LANCO";
                            
                            //Display the statement for menu option 4 and convert the artist name to all uppercase
                            System.out.println("");
                            System.out.println("You are now listening to " + stationMenu.toUpperCase()  + "!");
                        }
                        
                        else if (stationNum == 5)
                        {
                            //Set the variable to the correct artist
                            stationMenu = "Old Dominion";
                            
                            //Display the statement for menu option 5 and convert the artist name to all uppercase
                            System.out.println("");
                            System.out.println("You are now listening to " + stationMenu.toUpperCase() + "!");
                        }
                        
                        else if (stationNum == 6)
                        {
                            //Set the variable to the correct artist
                            stationMenu = "Piston Annies";
                            
                            //Display the statement for menu option 6 and convert the artist name to all uppercase
                            System.out.println("");
                            System.out.println("You are now listening to " + stationMenu.toUpperCase() + "!");
                        }
                        
                        else if (stationNum == 7)
                        {
                            //Set the variable to the correct artist
                            stationMenu = "Dierks Bentley";
                            
                            //Display the statement for menu option 7 and convert the artist name to all uppercase
                            System.out.println("");
                            System.out.println("You are now listening to " + stationMenu.toUpperCase() + "!");
                        }
                        
                        else if (stationNum == 8)
                        {
                            //Set the variable to the correct artist
                            stationMenu = "Carly Pearce";
                            
                            //Display the statement for menu option 8 and convert the artist name to all uppercase
                            System.out.println("");
                            System.out.println("You are now listening to " + stationMenu.toUpperCase() + "!");
                        }
                        
                        else if (stationNum == 9)
                        {
                            //Set the variable to the correct artist
                            stationMenu = "Billy Currington";
                            
                            //Display the statement for menu option 9 and convert the artist name to all uppercase
                            System.out.println("");
                            System.out.println("You are now listening to " + stationMenu.toUpperCase()  + "!");
                        }
                        
                        else if (stationNum == 10)
                        {
                            //Set the variable to the correct artist
                            stationMenu = "Eric Church";
                            
                            //Display the statement for menu option 10 and convert the artist name to all uppercase
                            System.out.println("");
                            System.out.println("You are now listening to " + stationMenu.toUpperCase() + "!");
                        }
                        
                    }
                    
                    break;
                    
                case 3:
                    //Create case 3 if user enters menu item 3
                    if (menuNum == 3)
                    {
                        //Display the statement for menu option 3
                        System.out.println("");
                        System.out.println("You have selected the EXIT Slacker Radio menu option.");
                    }
                    
                    break;
            }
  
        //Display closing message
        System.out.println(lname + "!" + "\tThank you for being a valued listener!");
        
        //Display Thank you message
        System.out.println("\n************************************GOODBYE Slacker Radio LISTENER***********************************");

  }//end of main
}//end of class